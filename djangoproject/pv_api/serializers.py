from django.contrib.auth.models import User, AnonymousUser
from django.forms import widgets
from rest_framework_gis import serializers
from pv_api.models import ParkingLot, HistoricalParkingLot, Fee, HistoricalFee, \
    AbstractParkingLot


class ParkingLotSerializer(serializers.GeoModelSerializer):
    class Meta:
        model = ParkingLot
        fields = ('id', 'name', 'address', 'point', 'area', 'capacity',
                  'is_guarded', 'opening_hour', 'closing_hour', 'description',
                  'is_accepted', 'is_deleted', 'timestamp', 'user', 'moderator',
        )
        read_only_fields = ('id', 'timestamp', 'is_accepted', 'user', 'moderator',)


class HistoricalParkingLotSerializer(serializers.GeoModelSerializer):
    class Meta:
        model = HistoricalParkingLot
        fields = ('id', 'name', 'address', 'point', 'area', 'capacity',
                  'is_guarded', 'opening_hour', 'closing_hour', 'description',
                  'is_accepted', 'is_deleted', 'timestamp', 'parking_lot',
                  'user', 'moderator',
        )
        read_only_fields = ('id', 'name', 'address', 'point', 'area',
                            'capacity', 'is_guarded', 'opening_hour',
                            'closing_hour', 'description', 'is_accepted',
                            'is_deleted', 'timestamp', 'parking_lot',
                            'user', 'moderator',
        )


class FeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fee
        fields = ('id', 'parking_lot', 'begin', 'end', 'price_h', 'price_desc',
                  'is_accepted', 'is_deleted', 'timestamp', 'user', 'moderator',
        )
        read_only_fields = ('id', 'timestamp', 'is_accepted', 'user', 'moderator',)


class HistoricalFeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = HistoricalFee
        fields = ('id', 'parking_lot', 'begin', 'end', 'price_h', 'price_desc',
                  'is_accepted', 'is_deleted', 'timestamp', 'fee', 'user',
                  'moderator',
        )
        read_only_fields = ('id', 'parking_lot', 'begin', 'end', 'price_h',
                            'price_desc', 'is_accepted', 'is_deleted',
                            'timestamp', 'fee', 'user', 'moderator',
        )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email')
        read_only_fields = ('username', )


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'password',)
        write_only_fields = ('password', )

    def restore_object(self, attrs, instance=None):
        user = User(username=attrs['username'], email=attrs['email'])
        user.set_password(attrs['password'])
        return user


class PasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('password',)
        write_only_fields = ('password',)
