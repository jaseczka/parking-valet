from django.conf.urls import patterns, url, include
from pv_api import views
from rest_framework import viewsets, routers
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = patterns('pv_api.views',
    url(r'^$', 'api_root'),
    url(r'^parkinglots/$', views.ParkingLotList.as_view(), name='parkinglot-list'),
    url(r'^parkinglots/(?P<pk>\d+)/$', views.ParkingLotDetail.as_view()),
    url(r'^fees/$', views.FeeList.as_view(), name='fee-list'),
    url(r'^fees/(?P<pk>\d+)/$', views.FeeDetail.as_view()),
    url(r'^parkinglots/historical/(?P<pk>\d+)/$',
        views.ParkingLotHistoricalParkingLotList.as_view()),
    url(r'^fees/historical/(?P<pk>\d+)/$',
        views.FeeHistoricalFeeList.as_view()),
    url(r'^historicalparkinglots/(?P<pk>\d+)/$',
        views.HistoricalParkingLotDetail.as_view()),
    url(r'^historicalfees/(?P<pk>\d+)/$',
        views.HistoricalFeeDetail.as_view()),

    # pattern /search/[lon]/[lat]/[rad]
    url(r'^(?P<lon>\d*\.?\d+)/(?P<lat>\d*\.?\d+)/(?P<rad>\d*\.?\d+)/$',
        views.SimpleRadiusSearch.as_view()),
    # pattern /search/[lon]/[lat]/[rad]/tags/[key]/[value]/+
    url(r'^(?P<lon>\d*\.?\d+)/(?P<lat>\d*\.?\d+)/(?P<rad>\d*\.?\d+)/tags/(?P<tags>.*)/$',
        views.AdvancedRadiusSearch.as_view()),
    # pattern /search/fees/[id]/
    url(r'^fees/parkinglot/(?P<pk>\d+)/$', views.ParkingLotFees.as_view()),
    url(r'^parkinglots/accept/(?P<pk>\d+)/$', views.AcceptParkingLot.as_view()),
    url(r'^parkinglots/reject/(?P<pk>\d+)/$', views.RejectParkingLot.as_view()),
    url(r'^fees/accept/(?P<pk>\d+)/$', views.AcceptFee.as_view()),
    url(r'^fees/reject/(?P<pk>\d+)/$', views.RejectFee.as_view()),
    url(r'^historicalparkinglots/restore/(?P<pk>\d+)/$',
        views.RestoreHistoricalParkingLot.as_view()),
    url(r'^historicalfees/restore/(?P<pk>\d+)/$',
        views.RestoreHistoricalFee.as_view()),
    url(r'^api-auth/', include('rest_framework.urls',
        namespace='rest_framework')),
    url(r'^users/register/$', views.RegisterUser.as_view()),
    url(r'^users/detail/(?P<username>.+)/$', views.UserDetail.as_view()),
    url(r'^users/newpassword/$', views.ChangePassword.as_view()),
)

urlpatterns += patterns('',
    url(r'^api-token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),
)

urlpatterns = format_suffix_patterns(urlpatterns)
