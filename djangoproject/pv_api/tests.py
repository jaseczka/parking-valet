from django.contrib.gis.geos.point import Point
from django.core.exceptions import ValidationError
from django.test import TestCase
from pv_api.models import Fee, ParkingLot, HistoricalParkingLot, HistoricalFee
from pv_api.db_wrapper import *
from pv_api.serializers import ParkingLotSerializer
from django.forms.models import model_to_dict
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer


class GetParkingLotsMethodTests(TestCase):
    def setUp(self):
        #wrzucamy do bazy dwa punkty
        pnt1 = Point(0, 0)
        pnt2 = Point(2, 2)
        pnt3 = Point(3, 3)
        ParkingLot(point=pnt1, is_guarded=True, is_accepted=True).save()
        ParkingLot(point=pnt2, is_guarded=True, is_accepted=None).save()
        ParkingLot(point=pnt3, is_guarded=None, is_accepted=None).save()

    def test_no_points(self):
        self.assertEqual(get_parking_lots(5, 5, 1).count(), 0)
    
    def test_one_point(self):
        self.assertEqual(get_parking_lots(0, 1, 1).count(), 1)

    def test_in_point_0_dist(self):
        self.assertEqual(get_parking_lots(0, 0, 0).count(), 1)

    def test_in_point_small_dist(self):
        self.assertEqual(get_parking_lots(0, 0, 1).count(), 1)

    def test_in_point_large_dist(self):
        self.assertEqual(get_parking_lots(2, 2, 10).count(), 3)

    def test_attributes(self):
        self.assertEqual(get_parking_lots(0, 0, 10, is_guarded=True).count(), 2)
        self.assertEqual(get_parking_lots(0, 0, 10, is_guarded=True,
                                          is_accepted=True).count(), 1)
        self.assertEqual(get_parking_lots(0, 0, 10).count(), 3)
        self.assertEqual(get_parking_lots(0, 0, 10, is_guarded=None).count(), 1)
        self.assertEqual(get_parking_lots(0, 0, 10, is_accepted=None).count(), 2)


class ParkingLotMethodsTest(TestCase):
    def setUp(self):
        pass

    def test_no_opening_hours(self):
        pl_before = ParkingLot.objects.count()
        p = Point(0, 0)
        pl = ParkingLot(point=p)

        try:
            pl.full_clean()
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception")

        pl.save()
        pl_after = ParkingLot.objects.count()

        self.assertEqual(pl_after, pl_before + 1)

    def test_partial_opening_hours(self):
        p = Point(0, 0)
        pl1 = ParkingLot(point=p, opening_hour=10)
        pl2 = ParkingLot(point=p, closing_hour=10)

        self.assertRaises(ValidationError, pl1.full_clean)
        self.assertRaises(ValidationError, pl2.full_clean)

    def test_invalid_full_opening_hours(self):
        p = Point(0, 0)
        pl1 = ParkingLot(point=p, opening_hour=10, closing_hour=10)
        pl2 = ParkingLot(point=p, opening_hour=10, closing_hour=7)

        self.assertRaises(ValidationError, pl1.full_clean)
        self.assertRaises(ValidationError, pl2.full_clean)

    def test_valid_full_opening_hours(self):
        pl_before = ParkingLot.objects.count()
        p = Point(0, 0)
        pl = ParkingLot(point=p, opening_hour=7, closing_hour=10)

        try:
            pl.full_clean()
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception")

        pl.save()
        pl_after = ParkingLot.objects.count()

        self.assertEqual(pl_after, pl_before + 1)

    def test_whole_day_opening_hours(self):
        p = Point(0, 0)
        pl = ParkingLot(point=p, opening_hour=0, closing_hour=24)

        try:
            pl.full_clean()
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception")


class FeeMethodsTest(TestCase):
    def setUp(self):
        pl1 = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24)
        pl2 = ParkingLot(point=Point(1, 1), name="pierwszy")
        pl1.save()
        pl2.save()

    def test_no_timespan(self):
        fee_before = Fee.objects.count()
        pl = ParkingLot.objects.get(name="zerowy")
        f = Fee(parking_lot=pl, price_h=0)

        try:
            f.full_clean()
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception")

        f.save()
        fee_after = Fee.objects.count()

        self.assertEqual(fee_after, fee_before + 1)

    def test_partial_timespan(self):
        pl = ParkingLot.objects.get(name="zerowy")
        f1 = Fee(parking_lot=pl, price_h=0, begin=10)
        f2 = Fee(parking_lot=pl, price_h=0, end=20)

        self.assertRaises(ValidationError, f1.full_clean)
        self.assertRaises(ValidationError, f2.full_clean)

    def test_invalid_full_timespan(self):
        pl = ParkingLot.objects.get(name="zerowy")
        f1 = Fee(parking_lot=pl, price_h=0, begin=10, end=10)
        f2 = Fee(parking_lot=pl, price_h=0, begin=22, end=20)

        self.assertRaises(ValidationError, f1.full_clean)
        self.assertRaises(ValidationError, f2.full_clean)

    def test_valid_full_timespan(self):
        fee_before = Fee.objects.count()
        pl = ParkingLot.objects.get(name="zerowy")
        f = Fee(parking_lot=pl, price_h=0, begin=10, end=12)

        try:
            f.full_clean()
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception")

        f.save()
        fee_after = Fee.objects.count()

        self.assertEqual(fee_after, fee_before + 1)

    def test_overlapping_timespans(self):
        pl = ParkingLot.objects.get(name="zerowy")
        f1 = Fee(parking_lot=pl, price_h=0, begin=10, end=16)
        f1.save()
        f2 = Fee(parking_lot=pl, price_h=0, begin=10, end=16)
        f3 = Fee(parking_lot=pl, price_h=0, begin=12, end=14)
        f4 = Fee(parking_lot=pl, price_h=0, begin=9, end=11)
        f5 = Fee(parking_lot=pl, price_h=0, begin=15, end=17)
        f6 = Fee(parking_lot=pl, price_h=0, begin=9, end=17)

        self.assertRaises(ValidationError, f2.full_clean)
        self.assertRaises(ValidationError, f3.full_clean)
        self.assertRaises(ValidationError, f4.full_clean)
        self.assertRaises(ValidationError, f5.full_clean)
        self.assertRaises(ValidationError, f6.full_clean)

    def test_non_overlapping_timespans(self):
        pl = ParkingLot.objects.get(name="zerowy")
        f1 = Fee(parking_lot=pl, price_h=0, begin=10, end=16)
        f1.save()
        f2 = Fee(parking_lot=pl, price_h=0, begin=9, end=10)
        f3 = Fee(parking_lot=pl, price_h=0, begin=16, end=17)
        f4 = Fee(parking_lot=pl, price_h=0, begin=6, end=8)
        f5 = Fee(parking_lot=pl, price_h=0, begin=18, end=20)

        try:
            f2.full_clean()
            f3.full_clean()
            f4.full_clean()
            f5.full_clean()
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception")

    def test_whole_day_timespan(self):
        pl = ParkingLot.objects.get(name="zerowy")
        f = Fee(parking_lot=pl, price_h=0, begin=0, end=24)

        try:
            f.full_clean()
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception")


class ChangeParkingLotMethodTest(TestCase):
    def setUp(self):
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24)
        pl.save()

    def test_simple_valid_change(self):
        z = ParkingLot.objects.get(name="zerowy", point=Point(0, 0))
        pl_before = ParkingLot.objects.count()
        hpl_before = HistoricalParkingLot.objects.count()
        try:
            change_parking_lot(z.id, name="zmieniony")
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception during change")
        pl_after = ParkingLot.objects.count()
        hpl_after = HistoricalParkingLot.objects.count()
        self.assertEqual(pl_before, pl_after)
        self.assertEqual(hpl_before + 1, hpl_after)
        try:
            pl = ParkingLot.objects.get(name="zmieniony")
            self.assertEqual(pl.id, z.id)
            self.assertEqual(pl.is_accepted, None)
        except ParkingLot.DoesNotExist:
            self.assertTrue(False, "DoesNotExist Exception for new info")

        try:
            hpl = HistoricalParkingLot.objects.get(name="zerowy")
            self.assertEqual(hpl.point, Point(0, 0))
        except HistoricalParkingLot.DoesNotExist:
            self.assertTrue(False, "DoesNotExist Exception for old info")

    def test_hours_valid_change(self):
        z = ParkingLot.objects.get(name="zerowy", point=Point(0, 0))
        pl_before = ParkingLot.objects.count()
        hpl_before = HistoricalParkingLot.objects.count()

        try:
            change_parking_lot(z.id, name="zmieniony", opening_hour=3)
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception during change")
        pl_after = ParkingLot.objects.count()
        hpl_after = HistoricalParkingLot.objects.count()
        self.assertEqual(pl_before, pl_after)
        self.assertEqual(hpl_before + 1, hpl_after)

        try:
            pl = ParkingLot.objects.get(name="zmieniony")
            self.assertEqual(pl.id, z.id)
            self.assertEqual(pl.opening_hour, 3)
            self.assertEqual(pl.closing_hour, 24)
        except ParkingLot.DoesNotExist:
            self.assertTrue(False, "DoesNotExist Exception for new info")

        try:
            hpl = HistoricalParkingLot.objects.get(name="zerowy")
            self.assertEqual(hpl.parking_lot, z)
            self.assertEqual(hpl.opening_hour, 0)
            self.assertEqual(hpl.closing_hour, 24)
        except HistoricalParkingLot.DoesNotExist:
            self.assertTrue(False, "DoesNotExist Exception for old info")

    def test_simple_invalid_change(self):
        z = ParkingLot.objects.get(name="zerowy", point=Point(0, 0))
        pl_before = ParkingLot.objects.count()
        hpl_before = HistoricalParkingLot.objects.count()

        with self.assertRaises(ValidationError):
            change_parking_lot(z.id, name="zmieniony", opening_hour=None)

        pl_after = ParkingLot.objects.count()
        hpl_after = HistoricalParkingLot.objects.count()
        self.assertEqual(pl_before, pl_after)
        self.assertEqual(hpl_before, hpl_after)
        pl = ParkingLot.objects.get(id=z.id)
        self.assertEqual(pl.opening_hour, 0)
        self.assertEqual(pl.name, "zerowy")

        with self.assertRaises(ParkingLot.DoesNotExist):
            ParkingLot.objects.get(name="zmieniony")

    def test_non_existent_parking_lot_change(self):
        pl_before = ParkingLot.objects.count()
        hpl_before = HistoricalParkingLot.objects.count()
        with self.assertRaises(ParkingLot.DoesNotExist):
            change_parking_lot(100, name="zmieniony")
        pl_after = ParkingLot.objects.count()
        hpl_after = HistoricalParkingLot.objects.count()
        self.assertEqual(pl_before, pl_after)
        self.assertEqual(hpl_before, hpl_after)


class DeleteParkingLotMethodTest(TestCase):
    def setUp(self):
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24)
        pl.save()

    def test_valid_delete(self):
        z = ParkingLot.objects.get(name="zerowy")
        pl_before = ParkingLot.objects.count()
        hpl_before = HistoricalParkingLot.objects.count()
        try:
            delete_parking_lot(z.id)
        except ParkingLot.DoesNotExist:
            self.assertTrue(False, "DoesNotExist Exception")
        pl_after = ParkingLot.objects.count()
        hpl_after = HistoricalParkingLot.objects.count()

        self.assertEqual(pl_before, pl_after)
        self.assertEqual(hpl_before + 1, hpl_after)

        pl = ParkingLot.objects.get(id=z.id)
        hpl = HistoricalParkingLot.objects.get(parking_lot=pl)

        self.assertEqual(pl.is_deleted, True)
        self.assertEqual(hpl.is_deleted, False)

    def test_invalid_delete(self):
        pl_before = ParkingLot.objects.count()
        hpl_before = HistoricalParkingLot.objects.count()
        with self.assertRaises(ParkingLot.DoesNotExist):
            delete_parking_lot(100)
        pl_after = ParkingLot.objects.count()
        hpl_after = HistoricalParkingLot.objects.count()

        self.assertEqual(pl_before, pl_after)
        self.assertEqual(hpl_before, hpl_after)


class AcceptParkingLotMethodTest(TestCase):
    def setUp(self):
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24)
        pl.save()

    def test_simple_accept(self):
        z = ParkingLot.objects.get(name="zerowy")
        pl_before = ParkingLot.objects.count()
        hpl_before = HistoricalParkingLot.objects.count()
        accept_parking_lot(z.id)
        pl_after = ParkingLot.objects.count()
        hpl_after = HistoricalParkingLot.objects.count()
        pl = ParkingLot.objects.get(id=z.id)
        hpl = HistoricalParkingLot.objects.get(parking_lot=pl)

        self.assertEqual(pl_before, pl_after)
        self.assertEqual(hpl_before + 1, hpl_after)
        self.assertEqual(pl.is_accepted, True)
        self.assertEqual(hpl.is_accepted, None)

    def test_accept_after_delete(self):
        z = ParkingLot.objects.get(name="zerowy")
        pl_before = ParkingLot.objects.count()
        hpl_before = HistoricalParkingLot.objects.count()
        # delete should create new historical pl and change existing pl
        delete_parking_lot(z.id)
        pl_after = ParkingLot.objects.count()
        hpl_after = HistoricalParkingLot.objects.count()
        self.assertEqual(pl_before, pl_after)
        self.assertEqual(hpl_before + 1, hpl_after)
        pl_before = pl_after
        hpl_before = hpl_after
        # accept should create new historical pl and delete existing pl
        accept_parking_lot(z.id)
        pl_after = ParkingLot.objects.count()
        hpl_after = HistoricalParkingLot.objects.count()
        self.assertEqual(pl_before - 1, pl_after)
        self.assertEqual(hpl_before + 1, hpl_after)


class GetFeesForParkingLotMethodTest(TestCase):
    def setUp(self):
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                        opening_hour=0, closing_hour=24)
        pl.save()
        f1 = Fee(parking_lot=pl, begin=0, end=6, price_h=3)
        f2 = Fee(parking_lot=pl, begin=6, end=18, price_h=6)
        f3 = Fee(parking_lot=pl, begin=18, end=24, price_h=4)
        f1.save()
        f2.save()
        f3.save()

    def test_get_fees(self):
        z = ParkingLot.objects.get(name="zerowy")
        fees = get_fees_for_parking_lot(z.id)
        self.assertEqual(fees.count(), 3)


class ParkingLotSerializerMethodsTest(TestCase):
    def setUp(self):
        pass

    def test_is_valid_nonvalid_opening_hours(self):
        pl1 = ParkingLot(point=Point(5, 5), opening_hour=10)
        pl2 = ParkingLot(point=Point(5, 5), closing_hour=10)
        pl3 = ParkingLot(point=Point(5, 5), opening_hour=10, closing_hour=10)
        s1 = ParkingLotSerializer(data=model_to_dict(pl1))
        s2 = ParkingLotSerializer(data=model_to_dict(pl2))
        s3 = ParkingLotSerializer(data=model_to_dict(pl3))
        self.assertFalse(s1.is_valid())
        self.assertFalse(s2.is_valid())
        self.assertFalse(s3.is_valid())

    def test_is_valid_without_point(self):
        pl = ParkingLot(name="nopoint")
        s = ParkingLotSerializer(data=model_to_dict(pl))
        self.assertFalse(s.is_valid())

    def test_is_valid_minimal_data(self):
        pl = ParkingLot(point=Point(0, 0))
        s = ParkingLotSerializer(data=model_to_dict(pl))
        self.assertTrue(s.is_valid())

class GetHistoricalParkingLotsMethodTest(TestCase):
    def setUp(self):        
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24)
        pl.save()

    def test_get_empty_history(self):
        pl = ParkingLot.objects.get(name="zerowy")
        self.assertEqual(0, get_historical_parking_lots(pl.id).count())     
     
    def test_get_simple_history(self):
        pl = ParkingLot.objects.get(name="zerowy")
        change_parking_lot(pl.id, name = "pierwszy")
        change_parking_lot(pl.id, name = "drugi")
        hist = get_historical_parking_lots(pl.id)
        self.assertEqual(hist.count(), 2)
        self.assertEqual(hist[0].parking_lot, pl)
        self.assertEqual(hist[1].parking_lot, pl)
        self.assertEqual(hist[0].name, "zerowy")
        self.assertEqual(hist[1].name, "pierwszy")

class DeleteFeeMethodTest(TestCase):
    def setUp(self):
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24)
        pl.save()
        fee = Fee(parking_lot=pl, price_h=0)
        fee.save()

    def test_valid_delete(self):
        pl = ParkingLot.objects.get(name="zerowy")
        fee = get_fees_for_parking_lot(pl.id)
        fee_before = Fee.objects.count()
        hfee_before = HistoricalFee.objects.count()
        delete_fee(fee[0].id)
        fee_after = Fee.objects.count()
        hfee_after = HistoricalFee.objects.count()

        self.assertEqual(fee_before, fee_after)
        self.assertEqual(hfee_before + 1, hfee_after)

        fee = Fee.objects.get(id=fee[0].id)
        hfee = HistoricalFee.objects.get(fee=fee)

        self.assertEqual(fee.is_deleted, True)
        self.assertEqual(hfee.is_deleted, False)


    def test_invalid_delete(self):
        fee_before = Fee.objects.count()
        hfee_before = HistoricalFee.objects.count()
        with self.assertRaises(Fee.DoesNotExist):
            delete_fee(100)
        fee_after = Fee.objects.count()
        hfee_after = HistoricalFee.objects.count()

        self.assertEqual(fee_before, fee_after)
        self.assertEqual(hfee_before, hfee_after)

class AcceptFeeMethodTest(TestCase):
    def setUp(self):
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24)
        pl.save()
        fee = Fee(parking_lot=pl, price_h=0)
        fee.save()

    def test_simple_accept(self):
        pl = ParkingLot.objects.get(name="zerowy")
        fee = get_fees_for_parking_lot(pl.id)
        fee_before = Fee.objects.count()
        hfee_before = HistoricalFee.objects.count()
        accept_fee(fee[0].id)
        fee_after = Fee.objects.count()
        hfee_after = HistoricalFee.objects.count()
        fee = Fee.objects.get(id=fee[0].id)
        hfee = HistoricalFee.objects.get(fee=fee)

        self.assertEqual(fee_before, fee_after)
        self.assertEqual(hfee_before + 1, hfee_after)
        self.assertEqual(fee.is_accepted, True)
        self.assertEqual(hfee.is_accepted, None)

    def test_accept_after_delete(self):
        pl = ParkingLot.objects.get(name="zerowy")
        fee = get_fees_for_parking_lot(pl.id)
        fee_before = Fee.objects.count()
        hfee_before = HistoricalFee.objects.count()
        # delete should create new historical fee and change existing fee
        delete_fee(fee[0].id)
        fee_after = Fee.objects.count()
        hfee_after = HistoricalFee.objects.count()
        self.assertEqual(fee_before, fee_after)
        self.assertEqual(hfee_before + 1, hfee_after)
        fee_before = fee_after
        hfee_before = hfee_after
        # accept should create new historical fee and delete existing fee
        accept_fee(fee[0].id)
        fee_after = Fee.objects.count()
        hfee_after = HistoricalFee.objects.count()
        self.assertEqual(fee_before - 1, fee_after)
        self.assertEqual(hfee_before + 1, hfee_after)

class ChangeFeeMethodTest(TestCase):
    def setUp(self):        
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24)
        pl.save()
        fee = Fee(parking_lot=pl, price_h=0, begin=1, end=10)
        fee.save()

    def test_simple_valid_change(self):
        pl = ParkingLot.objects.get(name="zerowy", point=Point(0, 0))
        fee = get_fees_for_parking_lot(pl.id)
        fee_before = Fee.objects.count()
        hfee_before = HistoricalFee.objects.count()
        try:
            change_fee(fee[0].id, price_h=10)
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception during change")
        fee_after = Fee.objects.count()
        hfee_after = HistoricalFee.objects.count()
        self.assertEqual(fee_before, fee_after)
        self.assertEqual(hfee_before + 1, hfee_after)
        try:
            fee_changed = Fee.objects.get(price_h=10)
            self.assertEqual(fee_changed.id, fee[0].id)
            self.assertEqual(fee_changed.is_accepted, None)
        except Fee.DoesNotExist:
            self.assertTrue(False, "DoesNotExist Exception for new info")
        try:
            hfee = HistoricalFee.objects.get(price_h=0)
        except Fee.DoesNotExist:
            self.assertTrue(False, "DoesNotExist Exception for old info")

    def test_non_existent_fee_change(self):
        fee_before = Fee.objects.count()
        hfee_before = HistoricalFee.objects.count()
        with self.assertRaises(Fee.DoesNotExist):
            change_fee(100, price_h=10)
        fee_after = Fee.objects.count()
        hfee_after = HistoricalFee.objects.count()
        self.assertEqual(fee_before, fee_after)
        self.assertEqual(fee_before, fee_after)



    def test_hours_valid_change(self):
        pl = ParkingLot.objects.get(name="zerowy", point=Point(0, 0))
        fee = get_fees_for_parking_lot(pl.id)
        fee_before = Fee.objects.count()
        hfee_before = HistoricalFee.objects.count()

        try:
            change_fee(fee[0].id, price_h=10, begin=3)
        except ValidationError:
            self.assertTrue(False, "ValidationError Exception during change")
        fee_after = Fee.objects.count()
        hfee_after = HistoricalFee.objects.count()
        self.assertEqual(fee_before, fee_after)
        self.assertEqual(hfee_before + 1, hfee_after)

        try:
            fee_changed = Fee.objects.get(price_h=10)
            self.assertEqual(fee_changed.id, fee[0].id)
            self.assertEqual(fee_changed.begin, 3)
            self.assertEqual(fee[0].end, 10)
        except Fee.DoesNotExist:
            self.assertTrue(False, "DoesNotExist Exception for new info")

        try:
            hpl = HistoricalFee.objects.get(price_h=0)
            self.assertEqual(hpl.fee, fee[0])
            self.assertEqual(hpl.begin, 1)
            self.assertEqual(hpl.end, 10)
        except HistoricalFee.DoesNotExist:
            self.assertTrue(False, "DoesNotExist Exception for old info")
    
    def test_simple_invalid_change(self):
        pl = ParkingLot.objects.get(name="zerowy", point=Point(0, 0))
        fee = get_fees_for_parking_lot(pl.id)
        fee_before = Fee.objects.count()
        hfee_before = HistoricalFee.objects.count()

        with self.assertRaises(ValidationError):
            change_fee(fee[0].id, price_h=10, begin=None)

        fee_after = Fee.objects.count()
        hfee_after = HistoricalFee.objects.count()
        self.assertEqual(fee_before, fee_after)
        self.assertEqual(hfee_before, hfee_after)
        fee_new = Fee.objects.get(id=fee[0].id)
        self.assertEqual(fee_new.begin, 1)
        self.assertEqual(fee_new.price_h, 0)

        with self.assertRaises(Fee.DoesNotExist):
            Fee.objects.get(begin=None)

class GetHistoricalFeesMethodTest(TestCase):
    def setUp(self):        
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24)
        pl.save()
        fee = Fee(parking_lot=pl, price_h=0, begin=1, end=10)
        fee.save()

    def test_get_empty_history(self):
        pl = ParkingLot.objects.get(name="zerowy")
        fee = get_fees_for_parking_lot(pl.id)[0]
        self.assertEqual(0, get_historical_fees(fee.id).count())     
   
     
    def test_get_simple_history(self):
        pl = ParkingLot.objects.get(name="zerowy")
        fee = get_fees_for_parking_lot(pl.id)[0]
        prev_price_h = fee.price_h
        change_fee(fee.id, price_h=10)
        change_fee(fee.id, price_h=12)
        hist = get_historical_fees(fee.id)
        self.assertEqual(hist.count(), 2)
        self.assertEqual(hist[0].fee, fee)
        self.assertEqual(hist[1].fee, fee)
        self.assertEqual(hist[0].price_h, 0)
        self.assertEqual(hist[1].price_h, 10)

class RejectParkingLotMethodTest(TestCase):
    def setUp(self):
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24,
                         is_accepted=True)
        pl.save()
     
    def test_single_reject(self):
        pl = ParkingLot.objects.get(name="zerowy")
        #dodaje wpisy w historii
        change_parking_lot(pl.id, name="pierwszy")
        change_parking_lot(pl.id, name="drugi")
        #sprawdzam czy wpisy sa w historii
        hist = get_historical_parking_lots(pl.id)
        self.assertEqual(hist[0].name, "zerowy")
        self.assertEqual(hist[0].is_accepted, True)
        self.assertEqual(hist[1].name, "pierwszy")
        self.assertEqual(hist[1].is_accepted, None)
        hist_before = hist.count()
        #sprawdzam czy aktualny parking lot jest ok
        cur_pl = ParkingLot.objects.get(name="drugi")
        self.assertEqual(cur_pl.point, Point(0,0))
        self.assertEqual(cur_pl.opening_hour, 0)
        self.assertEqual(cur_pl.closing_hour, 24)
        #odrzucam aktualny parking lot
        reject_parking_lot(cur_pl.id)
        #sprawdzam czy teraz jest dobry parking lot
        cur_pl = ParkingLot.objects.get(name="pierwszy")
        self.assertEqual(cur_pl.point, Point(0,0))
        self.assertEqual(cur_pl.opening_hour, 0)
        self.assertEqual(cur_pl.closing_hour, 24)
        self.assertEqual(cur_pl.is_accepted, None)
        #sprawdzam czy teraz historia jest dobra
        hist = get_historical_parking_lots(cur_pl.id)
        hist_after = hist.count()
        self.assertEqual(hist_before + 1, hist_after)
        self.assertEqual(hist[0].name, "zerowy")
        self.assertEqual(hist[1].name, "pierwszy")
        self.assertEqual(hist[2].name, "drugi")
        self.assertEqual(hist[2].point, Point(0,0))
        self.assertEqual(hist[2].opening_hour, 0)
        self.assertEqual(hist[2].closing_hour, 24)
        self.assertEqual(hist[2].parking_lot, cur_pl)
        self.assertEqual(hist[2].is_accepted, False)
        #sprawdzam czy timestampy dobrze sie zapisaly
        self.assertTrue(True, hist[2].timestamp > hist[1].timestamp) 
        self.assertTrue(True, hist[1].timestamp > hist[0].timestamp)

class TestRestoreParkingLotMethod(TestCase):
    def setUp(self):
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24,
                         is_accepted=True)
        pl.save()

    def test_simple_restore(self):
        pl = ParkingLot.objects.get(name="zerowy")
        change_parking_lot(pl.id, name="pierwszy")
        change_parking_lot(pl.id, name="drugi")
        change_parking_lot(pl.id, name="trzeci")
        hqs_before = HistoricalParkingLot.objects.all() 
        hpl_before = HistoricalParkingLot.objects.count()
        hpl = HistoricalParkingLot.objects.get(name="drugi")
        restore_parking_lot(hpl.id)
        pl = ParkingLot.objects.get(name="drugi")
        hpl_after = HistoricalParkingLot.objects.count()
        self.assertEqual(hpl_before + 1, hpl_after)
        hqs_after = HistoricalParkingLot.objects.all()
        self.assertEqual(hqs_before[0], hqs_after[0])
        self.assertEqual(hqs_before[1], hqs_after[1])
        self.assertEqual(hqs_before[2], hqs_after[2])
        self.assertEqual(hqs_after[3].name, "trzeci")
        self.assertEqual(hqs_after[3].parking_lot, pl)
        
 

class RejectFeeMethodTest(TestCase):
    def setUp(self):
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24,
                         is_accepted=True)
        pl.save()
        fee = Fee(parking_lot=pl, price_h=0, begin=1, end=10, is_accepted=True)
        fee.save()
     
    def test_single_reject(self):
        pl = ParkingLot.objects.get(name="zerowy")
        fee = get_fees_for_parking_lot(pl.id)[0]
        #dodaje wpisy w historii
        change_fee(fee.id, price_h=10)
        change_fee(fee.id, price_h=20)
        #sprawdzam czy wpisy sa w historii
        hist = get_historical_fees(fee.id)
        self.assertEqual(hist[0].price_h, 0)
        self.assertEqual(hist[0].is_accepted, True)
        self.assertEqual(hist[1].price_h, 10)
        self.assertEqual(hist[1].is_accepted, None)
        hist_before = hist.count()
        #sprawdzam czy aktualne fee jest ok
        cur_fee = Fee.objects.get(price_h=20)
        self.assertEqual(cur_fee.begin, 1)
        self.assertEqual(cur_fee.end, 10)
        #odrzucam aktualne fee
        reject_fee(cur_fee.id)
        #sprawdzam czy teraz jest dobre fee
        cur_fee = Fee.objects.get(price_h=10)
        self.assertEqual(cur_fee.begin, 1)
        self.assertEqual(cur_fee.end, 10)
        self.assertEqual(cur_fee.is_accepted, None)
        #sprawdzam czy teraz historia jest dobra
        hist = get_historical_fees(cur_fee.id)
        hist_after = hist.count()
        self.assertEqual(hist_before + 1, hist_after)
        self.assertEqual(hist[0].price_h, 0)
        self.assertEqual(hist[1].price_h, 10)
        self.assertEqual(hist[2].price_h, 20)
        self.assertEqual(hist[2].begin, 1)
        self.assertEqual(hist[2].end, 10)
        self.assertEqual(hist[2].fee, cur_fee)
        self.assertEqual(hist[2].is_accepted, False)
        #sprawdzam czy timestampy dobrze sie zapisaly
        self.assertTrue(True, hist[2].timestamp > hist[1].timestamp) 
        self.assertTrue(True, hist[1].timestamp > hist[0].timestamp)

class TestRestoreFee(TestCase):
    def setUp(self):
        pl = ParkingLot(point=Point(0, 0), name="zerowy",
                         opening_hour=0, closing_hour=24,
                         is_accepted=True)
        pl.save()

    def test_simple_restore(self):
        pl = ParkingLot.objects.get(name="zerowy")
        change_parking_lot(pl.id, name="pierwszy")
        change_parking_lot(pl.id, name="drugi")
        change_parking_lot(pl.id, name="trzeci")
        hqs_before = HistoricalParkingLot.objects.all() 
        hpl_before = HistoricalParkingLot.objects.count()
        hpl = HistoricalParkingLot.objects.get(name="drugi")
        restore_parking_lot(hpl.id)
        pl = ParkingLot.objects.get(name="drugi")
        hpl_after = HistoricalParkingLot.objects.count()
        self.assertEqual(hpl_before + 1, hpl_after)
        hqs_after = HistoricalParkingLot.objects.all()
        self.assertEqual(hqs_before[0], hqs_after[0])
        self.assertEqual(hqs_before[1], hqs_after[1])
        self.assertEqual(hqs_before[2], hqs_after[2])
        self.assertEqual(hqs_after[3].name, "trzeci")
        self.assertEqual(hqs_after[3].parking_lot, pl)
   
