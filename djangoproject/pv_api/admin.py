from django.contrib import admin
from django.contrib.gis.db import models
from pv_api.models import ParkingLot, Fee
from django.forms.widgets import Textarea
from django.contrib.auth.models import Permission

admin.site.register(Permission)


# Register your models here.
class FeeInline(admin.TabularInline):
    model = Fee
    extra = 1


class ParkingLotAdmin(admin.ModelAdmin):
    #fields = ['name', 'address', 'point', 'area', 'opening_hour', 'closing_hour',
    #          'capacity', 'is_guarded', 'description']
    fieldsets = [
        (None, {'fields': ['name']}),
        ('Location', {'fields': ['address', 'point']}),
        ('Exact Area', {'fields': ['area'], 'classes': ['collapse']}),
        ('Description', {'fields': ['capacity', 'is_guarded', ('opening_hour',
                                    'closing_hour'), 'description']}),
        ('Status', {'fields': ['is_accepted', 'is_deleted']}),
    ]

    inlines = [FeeInline]

    list_display = ('name', 'address', 'point', 'opening_hour', 'closing_hour',
                    'capacity', 'is_guarded', 'description', 'is_accepted',
                    'is_deleted')
    list_display_links = ('name', 'point')
    search_fields = ('name', 'address', 'description')
    list_filter = ('capacity', 'is_guarded', 'is_accepted', 'is_deleted')

    # tekst zamiast mapki
    formfield_overrides = {
        models.PointField: {'widget': Textarea}
    }


admin.site.register(ParkingLot, ParkingLotAdmin)
