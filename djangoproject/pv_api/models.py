# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.core.exceptions import ValidationError


def _make_historical_parking_lot(pl):
    hpl = HistoricalParkingLot(parking_lot=pl,
                               timestamp=pl.timestamp,
                               name=pl.name, address=pl.address,
                               point=pl.point, area=pl.area,
                               capacity=pl.capacity,
                               is_guarded=pl.is_guarded,
                               opening_hour=pl.opening_hour,
                               closing_hour=pl.closing_hour,
                               description=pl.description,
                               is_accepted=pl.is_accepted,
                               is_deleted=pl.is_deleted,
                               user=pl.user,
                               moderator=pl.moderator)
    return hpl


# Create your models here.
class AbstractParkingLot(models.Model):
    class Meta:
        abstract = True

    U5, U20, U50, U100, U500, A500 = range(6)

    Capacity = (
        (U5, 'Under 5'),
        (U20, '5 to 20'),
        (U50, '20 to 50'),
        (U100, '50 to 100'),
        (U500, '100 to 500'),
        (A500, '500 & Above'),
    )

    name = models.CharField(max_length=50, blank=True)
    address = models.TextField(blank=True)
    point = models.PointField(verbose_name='figurative point')
    area = models.PolygonField(verbose_name='exact polygon area',
                               null=True, blank=True)
    capacity = models.PositiveIntegerField(verbose_name='capacity category',
                                   choices=Capacity, null=True, blank=True)
    is_guarded = models.NullBooleanField()
    opening_hour = models.IntegerField(choices=[(x, x) for x in range(0, 24)],
                                       null=True, blank=True)
    closing_hour = models.IntegerField(choices=[(x, x) for x in range(1, 25)],
                                       null=True, blank=True)
    description = models.TextField(blank=True)
    is_accepted = models.NullBooleanField()
    is_deleted = models.BooleanField(default=False)
    user = models.ForeignKey(User, related_name='%(class)s_edited',
                             null=True, blank=True, on_delete=models.SET_NULL)
    moderator = models.ForeignKey(User, related_name='%(class)s_moderated',
                                  null=True, blank=True, on_delete=models.SET_NULL)

    objects = models.GeoManager()

    def __unicode__(self):
        return unicode(self.pk) + ": " + unicode(self.name) + " " + \
               unicode(self.point)

    def clean(self):
        if (self.opening_hour is None and self.closing_hour is not None) \
                or (self.opening_hour is not None and self.closing_hour is None):
            raise ValidationError('Incorrect timespan')
        if self.opening_hour is not None and self.closing_hour is not None \
                and self.opening_hour >= self.closing_hour:
            raise ValidationError('Incorrect timespan')


def _find_prev_pl(id):
    pl = ParkingLot.objects.get(id=id)
    qs = HistoricalParkingLot.objects.filter(parking_lot=pl)
    qs = qs.order_by('timestamp')
    #szukam poprzedniego parking lota
    prev = None
    for h in qs:
        if (h.timestamp < pl.timestamp) and (h.is_accepted != False):
            prev = h
    return prev


class ParkingLot(AbstractParkingLot):
    # przy każdej modyfikacji pole timestamp jest aktualizowane
    timestamp = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        # Odrzucenie edycji
        if self.is_accepted is False:
            # jezeli to pierwszy taki wpis to go nie zapisuje
            if self.pk is not None:
                pl = ParkingLot.objects.get(pk=self.pk)
                hpl = _make_historical_parking_lot(pl)
                hpl.is_accepted = False
                hpl.save()
                prev = _find_prev_pl(pl.id)
                if prev is not None:
                    fields = iter(ParkingLot._meta.fields)
                    self.name=prev.name
                    self.address=prev.address 
                    self.point=prev.point 
                    self.area=prev.area
                    self.capacity=prev.capacity 
                    self.is_guarded=prev.is_guarded
                    self.opening_hour=prev.opening_hour
                    self.closing_hour=prev.closing_hour
                    self.description=prev.description
                    self.is_accepted=prev.is_accepted
                    self.is_deleted=prev.is_deleted
                    super(ParkingLot, self).save(*args, **kwargs)
                #TODO co gdy jest None
        else:
            # Zapis nowego ParkingLot
            if self.pk is None:
                super(ParkingLot, self).save(*args, **kwargs)
            # Zapis edytowanego ParkingLot
            else:
                pl = ParkingLot.objects.get(pk=self.pk)
                hpl = _make_historical_parking_lot(pl)
                hpl.save()
                super(ParkingLot, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pl = ParkingLot.objects.get(pk=self.pk)
        hpl = _make_historical_parking_lot(pl)
        hpl.save()
        # Odlozenie do historii niektualnych juz fee
        for f in pl.fee_set.all():
            f.delete()
        super(ParkingLot, self).delete(*args, **kwargs)


class HistoricalParkingLot(AbstractParkingLot):
    # timestamp wykonanej modyfikacji != utworzenie rekordu w historii
    timestamp = models.DateTimeField()
    parking_lot = models.ForeignKey(ParkingLot, on_delete=models.DO_NOTHING)


def _make_historical_fee(fee):
    hfee = HistoricalFee(fee=fee,
                         timestamp=fee.timestamp,
                         parking_lot=fee.parking_lot, 
                         begin=fee.begin, end=fee.end,
                         price_h=fee.price_h,
                         price_desc=fee.price_desc,
                         is_accepted=fee.is_accepted,
                         is_deleted=fee.is_deleted)
    return hfee


class AbstractFee(models.Model):
    class Meta:
        abstract = True

    parking_lot = models.ForeignKey(ParkingLot, on_delete=models.DO_NOTHING)
    begin = models.IntegerField(verbose_name='begins',
                                choices=[(x, x) for x in range(0, 24)],
                                null=True, blank=True)
    end = models.IntegerField(verbose_name='ends',
                              choices=[(x, x) for x in range(1, 25)],
                              null=True, blank=True)
    price_h = models.PositiveIntegerField(verbose_name='price per hour')
    price_desc = models.TextField(verbose_name='price description', blank=True)
    is_accepted = models.NullBooleanField()
    is_deleted = models.BooleanField(default=False)
    user = models.ForeignKey(User, related_name='%(class)s_edited',
                             null=True, blank=True, on_delete=models.SET_NULL)
    moderator = models.ForeignKey(User, related_name='%(class)s_moderated',
                                  null=True, blank=True, on_delete=models.SET_NULL)

    def clean(self):
        if (self.begin is not None and self.end is None) \
                or (self.begin is None and self.end is not None):
            raise ValidationError('Incorrect timespan')

        if self.begin is not None and self.end is not None:
            if self.begin >= self.end:
                raise ValidationError('Incorrect timespan')
            fee_set = Fee.objects.filter(parking_lot=self.parking_lot)
            fee_set = fee_set.exclude(begin__gte=self.end)
            fee_set = fee_set.exclude(end__lte=self.begin)
            if self.id is not None:
                fee_set = fee_set.exclude(id=self.id)
            if fee_set.count() > 0:
                raise ValidationError('Overlapping timespans')

    def __unicode__(self):
        return unicode(self.parking_lot.pk) + ", between hs: " + unicode(self.begin) + " - " + \
               unicode(self.end) + ", price: " + unicode(self.price_h)


def _find_prev_fee(id):
    fee = Fee.objects.get(id=id)
    qs = HistoricalFee.objects.filter(fee=fee)
    qs = qs.order_by('timestamp')
    #szukam poprzedniego parking lota
    prev = None
    for h in qs:
        if (h.timestamp < fee.timestamp) and (h.is_accepted != False):
            prev = h
    return prev


class Fee(AbstractFee):
    timestamp = models.DateTimeField(auto_now=True)
    
    def save(self, *args, **kwargs):
        # Zapis nowego Fee
        if self.is_accepted is False:
            if self.pk is not None:
                fee = Fee.objects.get(pk=self.pk)
                hfee = _make_historical_fee(fee)
                hfee.is_accepted = False
                hfee.save()
                prev = _find_prev_fee(fee.id)
                if prev is not None:
                    self.parking_lot = prev.parking_lot
                    self.begin = prev.begin
                    self.end = prev.end
                    self.price_h = prev.price_h
                    self.price_desc = prev.price_desc
                    self.is_accepted = prev.is_accepted
                    self.is_deleted = prev.is_deleted
                    super(Fee, self).save(*args, **kwargs)
        else:
            if self.pk is None:
                super(Fee, self).save(*args, **kwargs)
            # Zapis edytowanego Fee
            else:
                fee = Fee.objects.get(pk=self.pk)
                hfee = _make_historical_fee(fee)
                hfee.save()
                super(Fee, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        fee = Fee.objects.get(pk=self.pk)
        hfee = _make_historical_fee(fee)
        hfee.save()
        super(Fee, self).delete(*args, **kwargs)


class HistoricalFee(AbstractFee):
    timestamp = models.DateTimeField()
    fee = models.ForeignKey(Fee, on_delete=models.DO_NOTHING)
