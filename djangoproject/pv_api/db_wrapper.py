from pv_api.models import ParkingLot, HistoricalParkingLot, Fee, HistoricalFee
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.core import serializers
from django.db import transaction
import bisect


# zwracam uwage, ze kwargsy moga miec ten sam styl co filter/exclude,
# czyli mozemy przekazywac takie klucze jak opening_hour__lte
def get_parking_lots(lon, lat, radius, **kwargs):
    pnt = Point(lon, lat)
    qs = ParkingLot.objects.filter(point__distance_lte=(pnt, radius))
    qs = qs.filter(**kwargs)
    return qs


def get_fees_for_parking_lot(id, **kwargs):
    pl = ParkingLot.objects.get(id=id)
    fees = pl.fee_set.all()
    fees = fees.filter(**kwargs)
    return fees


def change_parking_lot(id, **kwargs):
    pl = ParkingLot.objects.get(id=id)
    fields = iter(ParkingLot._meta.fields)
    mods = [(f.attname, kwargs[f.attname]) for f in fields if f.attname in kwargs]
    for fname, fval in mods:
        setattr(pl, fname, fval)
    pl.is_accepted = None
    pl.full_clean()
    pl.save()


def create_parking_lot(**kwargs):
    pl = ParkingLot()
    fields = iter(ParkingLot._meta.fields)
    mods = [(f.attname, kwargs[f.attname]) for f in fields if f.attname in kwargs]
    for fname, fval in mods:
        setattr(pl, fname, fval)

    pl.full_clean()
    pl.save()


def get_historical_parking_lots(id):
    pl = ParkingLot.objects.get(id=id)
    qs = HistoricalParkingLot.objects.filter(parking_lot=pl)
    return qs


def delete_parking_lot(id):
    change_parking_lot(id, is_deleted=True)


def accept_parking_lot(id):
    pl = ParkingLot.objects.get(id=id)
    pl.is_accepted = True

    if pl.is_deleted:
        pl.delete()
        return None
    else:
        pl.save()
        return pl


def reject_parking_lot(id):
    pl = ParkingLot.objects.get(id=id)
    pl.is_accepted = False
    pl.save()


def restore_parking_lot(id):
    hpl = HistoricalParkingLot.objects.get(id=id)
    pl_id = hpl.parking_lot.id
    change_parking_lot(pl_id, name=hpl.name, address=hpl.address, point=hpl.point,
                       area=hpl.area, capacity=hpl.capacity, is_guarded=hpl.is_guarded,
                       opening_hour=hpl.opening_hour, closing_hour=hpl.closing_hour,
                       description=hpl.description, is_accepted=hpl.is_accepted, 
                       is_deleted=hpl.is_deleted)
    return ParkingLot.objects.get(id=pl_id)


# funkcje modyfikujace, usuwajace, dodawajace fee
def change_fee(id, **kwargs):
    fee = Fee.objects.get(id=id)
    fields = iter(Fee._meta.fields)
    mods = [(f.attname, kwargs[f.attname]) for f in fields if f.attname in kwargs]
    for fname, fval in mods:
        setattr(fee, fname, fval)
    fee.is_accepted = None
    fee.full_clean()
    fee.save()


def create_fee(**kwargs):
    fee = Fee()
    fields = iter(Fee._meta.fields)
    mods = [(f.attname, kwargs[f.attname]) for f in fields if f.attname in kwargs]
    for fname, fval in mods:
        setattr(fee, fname, fval)

    fee.full_clean()
    fee.save()


def delete_fee(id):
    change_fee(id, is_deleted=True)


def accept_fee(id):
    fee = Fee.objects.get(id=id)
    fee.is_accepted = True

    if fee.is_deleted:
        fee.delete()
        return None
    else:
        fee.save()
        return fee


def get_historical_fees(id):
    fee = Fee.objects.get(id=id)
    qs = HistoricalFee.objects.filter(fee=fee)
    return qs


def reject_fee(id):
    fee = Fee.objects.get(id=id)
    fee.is_accepted = False
    fee.save()


def restore_fee(id):
    hfee = HistoricalFee.objects.get(id=id)
    fee_id = hfee.fee.id
    change_fee(fee_id, parking_lot=fee.parking_lot, begin=fee.begin, end=fee.end,
               price_h=fee.price_h, price_desc=fee.price_desc,
               is_accepted=hpl.is_accepted, is_deleted=hpl.is_deleted)
    return Fee.objects.get(id=fee_id)



