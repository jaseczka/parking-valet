from django.contrib.auth.models import User
from rest_framework import permissions


class IsModerator(permissions.BasePermission):

    def has_permission(self, request, view):
        print request.user.has_perm('Can change parking lot')
        return request.user.has_perm('pv_api.change_parkinglot') 


class IsProfileOwner(permissions.BasePermission):

    def has_permission(self, request, view):
        user = User.objects.get(username=view.kwargs['username'])
        return user.id == request.user.id

    def has_object_permission(self, request, view, obj):
        if obj is not None:
            user_id = obj.id
            return request.user.id == user_id
        return False