from django.contrib.auth.models import User
from django.http import Http404
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from pv_api.db_wrapper import get_parking_lots, get_fees_for_parking_lot, \
    accept_parking_lot, reject_parking_lot, accept_fee, reject_fee#, \
    #restore_parking_lot, restore_fee
from pv_api.serializers import ParkingLotSerializer, FeeSerializer, \
    HistoricalParkingLotSerializer, HistoricalFeeSerializer, UserSerializer, \
    CreateUserSerializer, PasswordSerializer
from pv_api.models import ParkingLot, Fee, HistoricalParkingLot, HistoricalFee
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework import permissions
from pv_api.permissions import IsModerator, IsProfileOwner


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'parking lot list': reverse('parkinglot-list', request=request, format=format),
        'fee list': reverse('fee-list', request=request, format=format),
    })


# POST Rejestracja nowego uzytkownika
class RegisterUser(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = CreateUserSerializer


# GET Dane uzytkownika
# PUT Zmiana danych uzytkownika (oprocz nazwy i hasla)
class UserDetail(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, IsProfileOwner, )
    lookup_field = 'username'


# PUT Zmiana hasla
class ChangePassword(generics.UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = PasswordSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def put(self, request, format=None):
        user = request.user
        if user.is_anonymous():
            return Response(status=status.HTTP_400_BAD_REQUEST)
        serializer = PasswordSerializer(user, data=request.DATA)
        if serializer.is_valid():
            user.set_password(serializer.init_data.get('password'))
            user.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_406_NOT_ACCEPTABLE)


class SimpleRadiusSearch(generics.GenericAPIView):

    def get_queryset(self, lon, lat, rad):
        try:
            return get_parking_lots(float(lon), float(lat), float(rad))
        except Exception:
            raise Http404

    def get(self, request, lon, lat, rad, format=None):
        qs = self.get_queryset(lon, lat, rad)
        serializer = ParkingLotSerializer(qs, many=True)
        return Response(serializer.data)


class AdvancedRadiusSearch(generics.GenericAPIView):

    def get_queryset(self, lon, lat, rad, tags):
        if tags.endswith('/'):
            tags = tags[:-1]
        words = tags.split('/')
        for i, w in enumerate(words):
            if w in [u'false', u'False']:
                words[i] = False
            elif w in [u'None', u'none', u'Null', u'null']:
                words[i] = None
        try:
            kwargs = dict(words[i:i+2] for i in range(0, len(words), 2))
            return get_parking_lots(float(lon), float(lat), float(rad), **kwargs)
        except Exception:
            raise Http404

    def get(self, request, lon, lat, rad, tags, format=None):
        qs = self.get_queryset(lon, lat, rad, tags)
        serializer = ParkingLotSerializer(qs, many=True)
        return Response(serializer.data)


# GET Lista wszystkich parkingow
# POST Stworzenie nowego parkingu
class ParkingLotList(generics.ListCreateAPIView):
    queryset = ParkingLot.objects.all()
    serializer_class = ParkingLotSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

    def pre_save(self, obj):
        obj.user = self.request.user
        # obj.moderator = None # domyslna wartosc dla nowego obiektu
        # obj.is_accepted = None # domyslna wartosc dla nowego obiektu


# GET Parking o zadanym pk
# PUT Edycja parkingu o zadanym pk
class ParkingLotDetail(generics.RetrieveUpdateAPIView):
    queryset = ParkingLot.objects.all()
    serializer_class = ParkingLotSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

    def pre_save(self, obj):
        obj.moderator = None
        obj.is_accepted = None


# GET Aktualne cenniki dla parkingu o zadanym pk
class ParkingLotFees(generics.GenericAPIView):

    def get_queryset(self, pk):
        return get_fees_for_parking_lot(pk)

    def get(self, request, pk, format=None):
        qs = self.get_queryset(pk)
        serializer = FeeSerializer(qs, many=True)
        return Response(serializer.data)


# POST (Moderator) Zaakceptowanie danych parkingu o zadanym pk
class AcceptParkingLot(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsModerator, )

    def get_object(self, pk):
        try:
            return accept_parking_lot(pk)
        except ParkingLot.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        pl = self.get_object(pk)
        if pl:
            serializer = ParkingLotSerializer(pl)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def pre_save(self, obj):
        obj.moderator = self.request.user


# POST (Moderator) Odrzucenie danych parkingu o zadanym pk
class RejectParkingLot(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsModerator, )

    def get_object(self, pk):
        try:
            return reject_parking_lot(pk)
        except ParkingLot.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        pl = self.get_object(pk)
        if pl:
            serializer = ParkingLotSerializer(pl)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def pre_save(self, obj):
        obj.moderator = self.request.user


# GET Historia parkingu o zadanym pk
class ParkingLotHistoricalParkingLotList(generics.GenericAPIView):

    def get_queryset(self, pk):
        try:
            pl = ParkingLot.objects.get(pk=pk)
            qs = HistoricalParkingLot.objects.filter(parking_lot=pl)
            return qs
        except Exception:
            raise Http404

    def get(self, request, pk, format=None):
        qs = self.get_queryset(pk)
        serializer = HistoricalParkingLotSerializer(qs, many=True)
        return Response(serializer.data)


# GET Punkt z historii parkingu o zadanym pk
class HistoricalParkingLotDetail(generics.RetrieveAPIView):
    queryset = HistoricalParkingLot.objects.all()
    serializer_class = HistoricalParkingLotSerializer


# POST Przywrocenie punktu o zadanym pk z historii parkingu
class RestoreHistoricalParkingLot(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsModerator, )

    def get_object(self, pk):
        try:
            return restore_parking_lot(pk)
        except ParkingLot.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        pl = self.get_object(pk)
        if pl:
            serializer = ParkingLotSerializer(pl)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def pre_save(self, obj):
        # Moderator przywracajacy punkt bedzie wpisany jako tworca edycji
        obj.user = self.request.user


# GET Lista wszystkich cennikow
# POST Dodanie nowego cennika
class FeeList(generics.ListCreateAPIView):
    queryset = Fee.objects.all()
    serializer_class = FeeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

    def pre_save(self, obj):
        obj.user = self.request.user
        # obj.moderator = None # domyslna wartosc dla nowego obiektu
        # obj.is_accepted = None # domyslna wartosc dla nowego obiektu


# GET Cennik o zadanym pk
# PUT Modyfikacja cennika o zadanym pk
class FeeDetail(generics.RetrieveUpdateAPIView):
    queryset = Fee.objects.all()
    serializer_class = FeeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

    def pre_save(self, obj):
        obj.user = self.request.user
        obj.moderator = None
        obj.is_accepted = None


# POST (Moderator) Zaakceptowanie danych cennika o zadanym pk
class AcceptFee(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsModerator, )

    def get_object(self, pk):
        try:
            return accept_fee(pk)
        except Fee.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        f = self.get_object(pk)
        if f:
            serializer = FeeSerializer(f)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def pre_save(self, obj):
        obj.moderator = self.request.user


# POST (Moderator) Odrzucenie danych cennika o zadanym pk
class RejectFee(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsModerator, )

    def get_object(self, pk):
        try:
            return reject_fee(pk)
        except Fee.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        f = self.get_object(pk)
        if f:
            serializer = ParkingLotSerializer(f)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def pre_save(self, obj):
        obj.moderator = self.request.user


# GET Historia cennika o zadanym pk
class FeeHistoricalFeeList(generics.GenericAPIView):

    def get_queryset(self, pk):
        try:
            f = Fee.objects.get(pk=pk)
            qs = HistoricalFee.objects.filter(fee=f)
            return qs
        except Exception:
            raise Http404

    def get(self, request, pk, format=None):
        qs = self.get_queryset(pk)
        serializer = HistoricalFeeSerializer(qs, many=True)
        return Response(serializer.data)


# GET Punkt z historii cennika o zadanym pk
class HistoricalFeeDetail(generics.RetrieveAPIView):
    queryset = HistoricalFee.objects.all()
    serializer_class = HistoricalFeeSerializer


# POST Przywrocenie punktu o zadanym pk z historii cennika
class RestoreHistoricalFee(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsModerator, )

    def get_object(self, pk):
        try:
            return restore_fee(pk)
        except ParkingLot.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        f = self.get_object(pk)
        if f:
            serializer = FeeSerializer(f)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def pre_save(self, obj):
        # Moderator przywracajacy bedzie wpisany jako tworca edycji
        obj.user = self.request.user

