# Description #

Academic, semester-long, 4-person project for Software Engineering Course. Aim of the project was to learn working with other people, managing the project (planning the work and meeting self-imposed deadlines), writing documentation.

Parking Valet is a RESTful API written in Python/Django for gathering and obtaining parking spots information. Database content is crowdsourced. Everybody who is authorized (logged in) may add or change existing information. A special group of users (moderators) designated by administrators may mark data as verified or delete false data. Everyone can access all the data, including non-verified by moderators or full history of changed records.

There is also a simple web client illustrating API functions.

More detailed project and API documentation is available in Polish.


## Development Environment ##
* Python 2.7
* Django 1.7
* Spatialite 2.4
* GEOS, GDAL, PROJ.4 Libraries
* [https://github.com/ottoyiu/django-cors-headers/](django-cors-headers)
* [http://www.django-rest-framework.org/](django-rest-framework)
* [https://github.com/djangonauts/django-rest-framework-gis](django-rest-framework-gis)

## Initialization ##
* [https://docs.djangoproject.com/en/1.7/ref/contrib/gis/install/spatialite/#create-spatialite-db](Create Spatialite DB)