var website = "http://localhost";
var port = "8000";
var token = '';

var icon_base = 'https://maps.google.com/mapfiles/kml/shapes/';
var map = null;
function initialize() {
	var latlng = new google.maps.LatLng(52.2296756, 21.012228700000037);
	map = new google.maps.Map(document.getElementById('map'), {
	      zoom: 13,
	      center: latlng,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    });

	google.maps.event.addListener(map, "rightclick", function(event)
            {
                place_cntr_marker(event.latLng);
                document.getElementById("lat").value = event.latLng.lat();
                document.getElementById("lon").value = event.latLng.lng();
                document.getElementById("lat_add").value = event.latLng.lat();
                document.getElementById("lon_add").value = event.latLng.lng();
            });
	
}
	var cntr_marker = null;
function place_cntr_marker(location){
		console.log('place marker ' + location.lat() + ' ' + location.lng());
		if (cntr_marker == null) {
  		    cntr_marker = new google.maps.Marker({
   			    position: location,
        		    map: map
     	            });
    		} else {
      			cntr_marker.setPosition(location);
    		}
		
	}    
google.maps.event.addDomListener(window, 'load', initialize);

function show_on_map(parking_lots) {
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for (i = 0; i < parking_lots.length; i++) {
        var pos = new google.maps.LatLng(parking_lots[i].point.coordinates[1], 
        	parking_lots[i].point.coordinates[0])
        marker = new google.maps.Marker({
            position: pos,
            map: map,
            icon: icon_base + 'parking_lot_maps.png'
        });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
        	return function() {
          		infowindow.setContent(parking_lots[i].name);
          		infowindow.open(map, marker);
        	}
      	})(marker, i));
    }
}

function create_table(parking_lots) {
    var str = '<table>';
    str += '<thead><tr>';
    str += '<th>Nazwa</th>';
    str += '<th>Położenie</th>';
    str += '<th>Adres</th>';
    str += '<th>Pojemność</th>';
    str += '<th>Godzina otwarcia</th>';
    str += '<th>Godzina zamknięcia</th>';
    str += '<th>Czy strzeżony</th>';
    str += '<th>Opis</th>';
    str += '<th>Czy zweryfikowany</th>';
    str += '</tr></thead>';

    str += '<tbody>';

    var obj;
    for (var i = 0; i < parking_lots.length; i++) {
        str += '<tr>';
        obj = parking_lots[i];
        str += '<td>' + obj.name + '</td>';
        str += '<td>' + obj.point.coordinates + '</td>';
        str += '<td>' + obj.address + '</td>';
        str += '<td>' + obj.capacity + '</td>';
        str += '<td>' + obj.opening_hour + '</td>';
        str += '<td>' + obj.closing_hour + '</td>';
        str += '<td>' + obj.is_guarded + '</td>';
        str += '<td>' + obj.description + '</td>';
        str += '<td>' + obj.is_accepted + '</td>';
        str += '</tr>';
    }
    str += '</tbody></table>';
    return str;
}

$(function () {
    $('#search').click(function () {
        var lat = $('#lat').val();
        var lon = $('#lon').val();
        var radius = $('#radius').val();
        var tags = '';
        if ($('#id_opening_hour').val() != '') {
            tags += 'opening_hour__lte/' + $('#id_opening_hour').val().toString() + '/';
        }
        if ($('#id_closing_hour').val() != '') {
            tags += 'closing_hour__gte/' + $('#id_closing_hour').val().toString() + '/';
        }
        if ($('#id_capacity').val() != '') {
            tags += 'capacity/' + $('#id_capacity').val().toString() + '/';
        }
        if ($('#id_is_guarded').val() != '') {
            tags += 'is_guarded/' + $('#id_is_guarded').val().toString() + '/';
        }
        if (tags != '') {
            tags = 'tags/' + tags;
        }
        window.request = $.ajax({
            url: website + ':' + port + '/search/' + lon.toString() + '/' + lat.toString() + '/' + radius.toString() + '/' + tags,
            type: "GET",
            cache: false,
            success: function (json) {
                $('#result').html(create_table(json));
				show_on_map(json);
				map.setCenter(new google.maps.LatLng(lat, lon));
				place_cntr_marker(new google.maps.LatLng(lat, lon));
            }
        });
        //console.log('simple_search');
    });
});

$(function () {
    $('#add_button').click(function () {
        var lat = $('#lat_add').val();
        var lon = $('#lon_add').val();
        var opening_hour = $('#id_opening_hour_add').val();
        var closing_hour = $('#id_closing_hour_add').val();
        var capacity = $('#id_capacity_add').val();
        //var is_guarded = $('#id_is_guarded_add')
        var name = $('#name_add').val();
        var address = $('#address_add').val()
        var description = $('#description_add').val()
        var data = new Object;
        data.name = name;
        data.address = address;
        data.capacity = capacity;
        //data.is_guarded = is_guarded;
        data.opening_hour = parseInt(opening_hour);
        data.closing_hour = closing_hour;
        data.description = description;
        data.point = new Object;
        data.point.type = "Point";
        data.point.coordinates = [parseFloat(lon),parseFloat(lat)]
        console.log(JSON.stringify(data));
        window.request = $.ajax({
            url: website + ':' + port + '/search/parkinglots/',
            type: "POST",
            headers: {
                Authorization: 'JWT '  + token.toString(),
                "Content-Type": "application/json"
            },
            data: JSON.stringify(data),
            cache: false,
            success: function (json) {
                $('#result').html("Dziękujemy za wkład w rozwój bazy danych parkingów!");

            },
            error: function (jqXHR, textStatus, errorThrown){
                //var string = jqXHR;
                alert('Formularz zawiera błędne dane lub nie jesteś zalogowany/a.')
                //$('#result').html(string);
            }
        });
    });
});


$(function () {
    $('#login_button').click(function () {
        var username = $('#username').val();
        var password = $('#password').val();
        console.log(username);
        console.log(password);
        var data = "username="+username.toString()+"&password="+password.toString();

        window.request = $.ajax({
            url: website + ':' + port + '/search/api-token-auth/',
            type: "POST",
            data: data,
            cache: false,
            success: function (json) {
                token = json.token;
                //$('#result').html(json.token);
                $('#login_container').html("Witaj, " + username + "!");
                console.log(token.toString());
            },
            error: function (jqXHR, textStatus, errorThrown){
                //var string = jqXHR;
                alert('Błąd logowania. Spróbuj jeszcze raz.')
                //$('#result').html(string);
            }
        });
    });
});
